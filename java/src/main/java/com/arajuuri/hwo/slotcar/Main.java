package com.arajuuri.hwo.slotcar;

import com.arajuuri.hwo.slotcar.message.Join;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(SlotCarBot.class);

    public static void main(String... args) {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        try {
            logger.info("Connecting");
            SlotCarBot bot = new SlotCarBot(host, port, new Join(botName, botKey));
            logger.info("Starting");
            bot.start();
        } catch (IOException ie) {

        }
    }

}
