/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.arajuuri.hwo.slotcar.message;

/**
 *
 * @author Siimai
 */
public class Turbo extends SendMsg {

    @Override
    protected String msgType() {
        return "turbo";
    }

    @Override
    protected Object msgData() {
        return "Kohti ääretöntä ja sen yli!";
    }
 
    
    
}
