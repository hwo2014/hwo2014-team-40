/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Siimai
 */
public class Race {

    private Track track;
    private List<Car> cars;
    private RaceSession raceSession;
    private String myName;
    private String myColor;
    private final Logger logger = LoggerFactory.getLogger(Race.class);
    private Stack<Double> angles = new Stack<Double>();
    private final static Double MAX_ANGLE = 55.0;
    private Double currentAngle = 0.0;
    private Double currentAngleDelta = 0.0;
    private Double inPieceDistance;
    private boolean carOnTrack = true;
    private Double turboDurationMilliseconds = 0.0;
    private Integer turboDurationTicks = 0;
    private Double turboFactor = 0.0;
    private Integer pieceIndex;
    private Integer currentLane;
    private Boolean changingLane = false;

    public Boolean isChangingLane() {
        return changingLane;
    }

    public void setChangingLane(Boolean changingLane) {
        this.changingLane = changingLane;
    }

    public Integer getTurboDurationTicks() {
        return turboDurationTicks;
    }

    public void setTurboDurationTicks(Integer turboDurationTicks) {
        this.turboDurationTicks = turboDurationTicks;
    }

    public Double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public void setTurboDurationMilliseconds(Double turboDurationMilliseconds) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    public Double getTurboFactor() {
        return turboFactor;
    }

    public void setTurboFactor(Double turboFactor) {
        this.turboFactor = turboFactor;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getMyColor() {
        return myColor;
    }

    public void setMyColor(String myColor) {
        this.myColor = myColor;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

    public void start() {

    }

    public void end() {

    }

    public void init() {

    }

    public void setMyCar() {

    }

    public double getSpeed() {
        Double speed = 1.0;
        Piece nextPiece = getNextPiece();
        if (carOnTrack) {
            if (currentAngleDelta > 0.0) {
                speed = 0.01;
            } else if (nextPiece != null && nextPiece.getAngleAbs() != null && nextPiece.getAngleAbs() > 30) {
                speed = 0.5;
            }
        }
        return speed;
    }

    private Piece getCurrentPiece() {
        if (track.getPieces() == null || track.getPieces().size() == 0) {
            return null;
        }
        return track.getPieces().get(pieceIndex);
    }

    private Piece getNextPiece() {
        if (track.getPieces() == null || track.getPieces().size() == 0) {
            return null;
        }
        if (pieceIndex + 1 >= track.getPieces().size()) {
            return track.getPieces().get(0);
        } else {
            return track.getPieces().get(pieceIndex + 1);
        }
    }

    private Integer straightPieceCount() {
        int counter = 0;
        List<Piece> rotated = new ArrayList<>();
        rotated.addAll(track.getPieces());
        Collections.rotate(rotated, -1 * pieceIndex);
        for (Piece piece : rotated) {
            if (piece.getAngle() == null || piece.getAngle() == 0.0) {
                counter++;
            } else {
                break;
            }
        }
        return counter;
    }

    public void updateCardPositions(CarPositions carPositions) {
        for (CarPosition carPosition : carPositions.getPositions()) {
            if (carPosition.getId().getColor().equals(myColor)) {
                currentAngleDelta = (carPosition.getAngle() - currentAngle) / currentAngle;
                currentAngle = carPosition.getAngle();

                if (carOnTrack) {
                    logger.debug("Current angle: " + currentAngle);
                }
                if (angles.empty() || currentAngle > angles.peek()) {
                    angles.push(currentAngle);
                }
                if (carPosition.getPiecePosition() != null) {
                    pieceIndex = carPosition.getPiecePosition().getPieceIndex();
                    inPieceDistance = carPosition.getPiecePosition().getInPieceDistance();
                    if (currentLane != carPosition.getPiecePosition().getLane().getStartLaneIndex()) {
                        currentLane = carPosition.getPiecePosition().getLane().getStartLaneIndex();
                        changingLane = false;
                    }
                }

            }
        }

    }

    public void crash(String color) {
        if (color.equals(myColor)) {
            carOnTrack = false;
        }
    }

    public void backOnTrack(String color) {
        if (color.equals(myColor)) {
            carOnTrack = true;
            currentAngle = 0.0;
        }
    }

    public void turboUsed() {
        setTurboDurationMilliseconds(0.0);
        setTurboDurationTicks(0);
        setTurboFactor(0.0);
    }

    public boolean shouldUseTurbo() {
        if (turboDurationMilliseconds > 0.0 && straightPieceCount() > 3) {
            return true;
        }
        return false;
    }

    public boolean shouldChangeLane() {
        if (changingLane) {
            return false;
        }
        Piece next = getNextPiece();
        if (next != null && next.isSwitch()) {
            return true;
        }
        return false;
    }

}
