/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.message;

/**
 *
 * @author Siimai
 */
public class JoinRace extends SendMsg {

    public final BotId botId;
    public final String trackName;
    public final Integer carCount;

    public JoinRace(final String name, final String key, final String trackName, final Integer carCount) {
        botId = new BotId(name, key);
        this.trackName = trackName;
        this.carCount = carCount;

    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

    public class BotId {

        public BotId(final String name, final String key) {
            this.key = key;
            this.name = name;
        }

        public final String name;
        public final String key;
    }
}
