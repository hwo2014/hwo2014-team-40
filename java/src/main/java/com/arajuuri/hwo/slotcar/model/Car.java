/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

/**
 *
 * @author Siimai
 */
public class Car {
    
    private Id id;
    private Dimensions dimensions;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }
    
    public class Dimensions {
        private Double length;
        private Double width;
        private Double guideFlagPosition;

        public Double getLength() {
            return length;
        }

        public void setLength(Double length) {
            this.length = length;
        }

        public Double getWidth() {
            return width;
        }

        public void setWidth(Double width) {
            this.width = width;
        }

        public Double getGuideFlagPosition() {
            return guideFlagPosition;
        }

        public void setGuideFlagPosition(Double guideFlagPosition) {
            this.guideFlagPosition = guideFlagPosition;
        }
        
        
    }

}
