/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Siimai
 */
public class Piece {

    private Double length;
    @SerializedName("switch")
    private Boolean _switch = false;

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Boolean isSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Double getAngle() {
        return angle;
    }

    public Double getAngleAbs() {
        if (angle != null) {
            return Math.abs(angle);
        }
        return null;

    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }
    private Integer radius;
    private Double angle;

}
