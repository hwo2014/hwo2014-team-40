/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

/**
 *
 * @author Siimai
 */
public class Data {

    private String name;
    private String key;
    private String color;
    private Double turboDurationMilliseconds;
    private Integer turboDurationTicks;
    private Double turboFactor;

    public Integer getTurboDurationTicks() {
        return turboDurationTicks;
    }

    public void setTurboDurationTicks(Integer turboDurationTicks) {
        this.turboDurationTicks = turboDurationTicks;
    }

    public Double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public void setTurboDurationMilliseconds(Double turboDurationMilliseconds) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    public Double getTurboFactor() {
        return turboFactor;
    }

    public void setTurboFactor(Double turboFactor) {
        this.turboFactor = turboFactor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    private Race race;

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

}
