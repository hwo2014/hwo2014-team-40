/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar;

import com.arajuuri.hwo.slotcar.message.Join;
import com.arajuuri.hwo.slotcar.message.JoinRace;
import com.arajuuri.hwo.slotcar.message.MsgWrapper;
import com.arajuuri.hwo.slotcar.message.Ping;
import com.arajuuri.hwo.slotcar.message.SendMsg;
import com.arajuuri.hwo.slotcar.message.Switch;
import com.arajuuri.hwo.slotcar.message.Throttle;
import com.arajuuri.hwo.slotcar.message.Turbo;
import com.arajuuri.hwo.slotcar.model.CarPositions;
import com.arajuuri.hwo.slotcar.model.Data;
import com.arajuuri.hwo.slotcar.model.Race;
import static com.google.common.io.ByteStreams.join;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Siimai
 */
public class SlotCarBot {

    private Gson gson = new Gson();
    private PrintWriter writer;
    private BufferedReader reader;
    private final Logger logger = LoggerFactory.getLogger(SlotCarBot.class);

    public SlotCarBot(String host, int port, Join join) throws IOException {
        final Socket socket = new Socket(host, port);
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        send(join);
    }

    public SlotCarBot(String host, int port, JoinRace joinRace) throws IOException {
        final Socket socket = new Socket(host, port);
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        send(joinRace);
    }

    public void start() throws IOException {
        String line = null;
        Race race = null;
        String name = null;
        String color = null;
        while ((line = reader.readLine()) != null) {
            try {
                MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
                if (msgFromServer.msgType.equals("carPositions")) {
                    CarPositions carPositions = gson.fromJson(line, CarPositions.class);
                    if (race != null) {
                        race.updateCardPositions(carPositions);
                        if (race.shouldChangeLane()) {
                            logger.info("Changing lane");
                            race.setChangingLane(true);
                            send(new Switch(Switch.RIGHT));
                        } else if (race.shouldUseTurbo()) {
                            logger.info("Used TURBO!");
                            race.turboUsed();
                            send(new Turbo());
                        } else {
                            Double speed = race.getSpeed();
                            logger.debug(
                                    "Current speed: " + speed);
                            send(new Throttle(speed));
                        }
                    } else {
                        send(new Throttle(0.5));
                    }
                } else if (msgFromServer.msgType.equals("join")) {
                    logger.info("Joined");
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    Data data = gson.fromJson(gson.toJson(msgFromServer.data), Data.class);
                    race = data.getRace();
                    race.setMyColor(color);
                    race.setMyName(name);
                    logger.info("Race init");
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    logger.info("Race end");
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    logger.info("Race start");
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("yourCar")) {
                    Data data = gson.fromJson(gson.toJson(msgFromServer.data), Data.class);
                    name = data.getName();
                    color = data.getColor();
                    send(new Ping());
                } else if (race != null && msgFromServer.msgType.equals("crash")) {
                    Data data = gson.fromJson(gson.toJson(msgFromServer.data), Data.class);
                    race.crash(data.getColor());
                    send(new Ping());
                } else if (race != null && msgFromServer.msgType.equals("spawn")) {
                    Data data = gson.fromJson(gson.toJson(msgFromServer.data), Data.class);
                    race.backOnTrack(data.getColor());
                    send(new Ping());
                } else if (race != null && msgFromServer.msgType.equals("turboAvailable")) {
                    Data data = gson.fromJson(gson.toJson(msgFromServer.data), Data.class);
                    race.setTurboDurationMilliseconds(data.getTurboDurationMilliseconds());
                    race.setTurboDurationTicks(data.getTurboDurationTicks());
                    race.setTurboFactor(data.getTurboFactor());
                    send(new Ping());
                } else {
                    send(new Ping());
                }
            } catch (Exception e) {
                send(new Ping());
            }
        }

    }

    private void send(SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

}
