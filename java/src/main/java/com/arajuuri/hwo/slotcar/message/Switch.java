/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.message;

/**
 *
 * @author Siimai
 */
public class Switch extends SendMsg {

    public static final String RIGHT = "Right";
    public static final String LEFT = "Left";

    private String direction;

    public Switch(String direction) {
        this.direction = direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    @Override
    protected Object msgData() {
        return direction;
    }

}
