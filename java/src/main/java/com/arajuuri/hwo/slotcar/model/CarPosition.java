/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

/**
 *
 * @author Siimai
 */
public class CarPosition {

    private Id id;
    private Double angle;
    private PiecePosition piecePosition;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

    public class PiecePosition {

        private Integer pieceIndex;
        private Double inPieceDistance;
        private Lane lane;
        private Integer lap;

        public Integer getPieceIndex() {
            return pieceIndex;
        }

        public void setPieceIndex(Integer pieceIndex) {
            this.pieceIndex = pieceIndex;
        }

        public Double getInPieceDistance() {
            return inPieceDistance;
        }

        public void setInPieceDistance(Double inPieceDistance) {
            this.inPieceDistance = inPieceDistance;
        }

        public Lane getLane() {
            return lane;
        }

        public void setLane(Lane lane) {
            this.lane = lane;
        }

        public Integer getLap() {
            return lap;
        }

        public void setLap(Integer lap) {
            this.lap = lap;
        }

    }

    public class Lane {

        private Integer startLaneIndex;
        private Integer endLaneIndex;

        public Integer getStartLaneIndex() {
            return startLaneIndex;
        }

        public void setStartLaneIndex(Integer startLaneIndex) {
            this.startLaneIndex = startLaneIndex;
        }

        public Integer getEndLaneIndex() {
            return endLaneIndex;
        }

        public void setEndLaneIndex(Integer endLaneIndex) {
            this.endLaneIndex = endLaneIndex;
        }
    }

}
