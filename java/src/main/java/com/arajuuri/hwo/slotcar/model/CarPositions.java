/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 *
 * @author Siimai
 */
public class CarPositions {

    @SerializedName("data")
    private List<CarPosition> positions;

    public List<CarPosition> getPositions() {
        return positions;
    }

    public void setPositions(List<CarPosition> positions) {
        this.positions = positions;
    }

}
