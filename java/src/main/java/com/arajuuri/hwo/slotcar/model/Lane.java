/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arajuuri.hwo.slotcar.model;

/**
 *
 * @author Siimai
 */
public class Lane {

    private Integer distanceFromCenter;
    private Integer index;

    public Integer getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(Integer distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

}
